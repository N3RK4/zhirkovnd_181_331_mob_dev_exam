#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFile>
#include <QString>
#include <QTextStream>
#include "func.h"
#include <QQmlContext>


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);



    QGuiApplication app(argc, argv);
    func func;


    QQmlApplicationEngine engine;
    QQmlContext *context = engine.rootContext();
    context->setContextProperty("func", &func);


    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);


    QObject::connect(engine.rootObjects().first(), SIGNAL(toSave(QString, QString)),
    &func, SLOT(toSave(QString, QString)));
    QObject::connect(engine.rootObjects().first(), SIGNAL(toSave2(QString)),
    &func, SLOT(toSave2(QString)));

    engine.load(QUrl(QStringLiteral("qrc:/Preview.qml")));




    return app.exec();
}
