import QtQuick 2.12
import QtQuick.Controls 2.5
import QtWebView 1.1
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.3


ApplicationWindow {
    visible: true
    width: 480
    height: 640
    title: qsTr("Web Redactor")

    signal toSave(string texttosave, string fp);
    signal toSave2(string texttosave);

    SwipeView {
        id: swipeView
        anchors.fill: parent
        interactive: false

        Redactor {
        }
        Preview {
        }

        Drawer{
            id: main_drawer
            width: parent.width * 0.5
            height: parent.height

            Rectangle{
                anchors.fill: parent
                color: "#00FFFFFF"
            }
            Image{
                id: imgpol
                width: parent.width * 0.7
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                source: "qrc:/Media/Icons/MPU_.jpg"
                fillMode: Image.PreserveAspectFit
            }
            Text {
                id: drtxt11
                color: "black"
                text: "HTML Редактор"
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.margins: 10
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 14
            }
            Text {
                id: drtxt1
                color: "black"
                text: "Экзаменационное задание по дисциплине \"Разработка безопасных мобильных приложений\". Московский Политех, 30 июня 2020г. "
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                anchors.left: parent.left
                anchors.top: drtxt11.bottom
                anchors.margins: 10
                width: parent.width*0.9
                wrapMode: TextArea.WordWrap
                font.family: "Helvetica"
                font.pointSize: 10
            }
            Text {
                id: hyperlink
                color: "black"
                text: "Repo: <a href='https://gitlab.com/N3RK4/zhirkovnd_181_331_mob_dev_exam'>Link to Repo</a>"
                onLinkActivated: Qt.openUrlExternally("https://gitlab.com/N3RK4/zhirkovnd_181_331_mob_dev_exam")
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 8
            }
            Text{
                id: author
                color: "black"
                text: "Автор: forgottenwork@yandex.ru"
                anchors.right: parent.right
                anchors.bottom: hyperlink.top
                anchors.margins: 10
                horizontalAlignment: Text.AlignRight
                width: parent.width*0.9
                font.family: "Helvetica"
                font.pointSize: 8
            }
        }
    }
}





