import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Dialogs 1.3


Page {
    Connections{
        target: func;
    }
    header: Item {
        width: parent.width
        height: 40
        Rectangle {
            anchors.fill: parent
            color: "#1588b5"
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: parent.height
                //            anchors.margins: 10

                id:h_btn_burger
                icon.source: "qrc:/Media/Icons/3lines.png"
                icon.color: "white"
                background: Rectangle {
                    color: h_btn_burger.pressed ? "#155a75":"#1588b5"
                }
                onClicked: {
                    main_drawer.open();
                }


            }
            Text {
                id: rtcg_h
                text: qsTr("Редактор")
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.bold: true
            }
        }


    }
    FileDialog {
        id: fileDialog

        folder : shortcuts.home
        title: qsTr("Open file")
        nameFilters: [qsTr("png files (*.png)"), qsTr("jpg files (*.jpg)")]
        onAccepted:{
            txa1.insert(txa1.cursorPosition,"<img src=\""+fileDialog.fileUrl+"\" alt=\"\">")
        }
        onRejected: {
            txa1.insert(txa1.cursorPosition,"<img src=\"\" alt=\"\">")
        }
    }

    FileDialog {
        id: fileDialog2

        folder : shortcuts.home
        title: qsTr("Open file")
        nameFilters: [qsTr("html files (*.html)")]
        onAccepted:{
            toSave(txa1.text, fileDialog2.fileUrl)
        }
        onRejected: {
            toSave(txa1.text, "NONE")
        }
    }
    ColorDialog {
        id: colorDialog
        title: "Please choose a color"
        onAccepted: {
            txa1.insert(txa1.cursorPosition,"color: "+colorDialog.color)
        }
        onRejected: {
            txa1.insert(txa1.cursorPosition,"color: ")
        }

    }



    ToolBar{
        width: parent.width
        height: 100
        id: tools
        background: Rectangle{
            color: "#00000000"
        }

        Button{
            id:save_btn
            //            text: "Save"
            icon.source: "qrc:/Media/Icons/save.png"
            icon.color: save_btn.pressed ? "#9ca2b0":"#15888b5"
            background: Rectangle{
                color: "#00000000"
            }
            anchors.left: parent.left
            onClicked:svmn.open()
            Menu{
                id: svmn

                MenuItem {
                    text: "Сохранить"
                    onClicked: toSave(txa1.text, "NONE");


                }
                MenuItem {
                    text: "Сохранить как"
                    onClicked: {fileDialog2.open()}


                }
            }


        }
        Button {
            id: prv_btn
            //            text: "PreView"
            icon.source: "qrc:/Media/Icons/search.png"
            icon.color: prv_btn.pressed ? "#9ca2b0":"#15888b5"
            background: Rectangle{
                color: "#00000000"
            }
            anchors.left: save_btn.right
            onClicked: {
                toSave2(txa1.text);

                swipeView.currentIndex = 1
            }
        }

    Button {
        id: mn_btn
        //            text: "PreView"
        icon.source: "qrc:/Media/Icons/plus.png"
        icon.color: mn_btn.pressed ? "#9ca2b0":"#15888b5"
        background: Rectangle{
            color: "#00000000"
        }
        anchors.left: prv_btn.right
        onClicked:addmn.open()
        Menu{
            id: addmn

            MenuItem {
                text: "h1"
                onClicked: txa1.insert(txa1.cursorPosition,"<h1></h1>")


            }
            MenuItem {
                text: "h2"
                onClicked: txa1.insert(txa1.cursorPosition,"<h2></h2>")


            }
            MenuItem {
                text: "div"
                onClicked: txa1.insert(txa1.cursorPosition,"<div></div>")


            }
            MenuItem {
                text: "img"
                onClicked: txa1.insert(txa1.cursorPosition,"<img src=\"\" alt=\"\">")


            }
            MenuItem {
                text: "img+src"
                onClicked: {
                    fileDialog.open();
                }


            }
            MenuItem {
                text: "a"

                onClicked: txa1.insert(txa1.cursorPosition,"<a href=\"\"></a>")


            }
            MenuItem {
                text: "color"
                onClicked: colorDialog.open()


            }
            MenuItem {
                text: "iclude bootstrap link"

                onClicked: txa1.insert(txa1.cursorPosition,"<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">")


            }
            MenuItem {
                text: "iclude bootstrap scripts"

                onClicked: txa1.insert(txa1.cursorPosition,"<script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\" integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\" crossorigin=\"anonymous\"></script>\n<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\" integrity=\"sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q\" crossorigin=\"anonymous\"></script>\n<script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\" integrity=\"sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl\" crossorigin=\"anonymous\"></script>")


            }
            MenuItem {
                text: "Html Document base"

                onClicked: txa1.insert(txa1.cursorPosition,"<!DOCTYPE html>\n<html lang=\"en\">\n<head>\n   <meta charset=\"UTF-8\">\n	<title>Document</title>\n</head>\n<body>\n  \n</body>\n</html>")


            }
        }


    }


    Switch {
        id: wrapcheck
        text: "Wrap"
        anchors.left: mn_btn.right

    }
    Label{
        id: font_lbl
        text: "Font size: "
        anchors.top: save_btn.bottom
        anchors.topMargin: 15
        anchors.leftMargin: 10
        font.pixelSize: 16
        anchors.left: parent.left
    }
    SpinBox {
        id: spnb_font
        anchors.left: font_lbl.right
        anchors.leftMargin: 5
        anchors.verticalCenter: font_lbl.verticalCenter
        value: 14
    }
    Button {
        id: cp_btn
        anchors.top: save_btn.bottom
        anchors.verticalCenter: font_lbl.verticalCenter
        text: "Copy"
        icon.source: "qrc:/Media/Icons/213.png"
        icon.color: cp_btn.pressed ? "#9ca2b0":"#15888b5"
        background: Rectangle{
            color: "#00000000"
        }
        anchors.left: spnb_font.right
        onClicked: {
            txa1.copy()
        }
    }
    Button {
        id: pst_btn
        anchors.top: save_btn.bottom
        anchors.verticalCenter: font_lbl.verticalCenter
        text: "Paste"
        icon.source: "qrc:/Media/Icons/321.png"
        icon.color: pst_btn.pressed ? "#9ca2b0":"#15888b5"
        background: Rectangle{
            color: "#00000000"
        }
        anchors.left: cp_btn.right
        onClicked: {
            txa1.insert(txa1.cursorPosition,txa1.paste())
        }


    }
    }



Item{
    id: itm1
    anchors.top: tools.bottom
    width: parent.width
    anchors.bottom: parent.bottom


    Flickable{
        id:flk1
        anchors.fill:parent
        flickableDirection: Flickable.HorizontalAndVerticalFlick
        clip: true
        contentHeight: itm1.height
        contentWidth: itm1.width
        TextArea.flickable: TextArea{
            id: txa1
            height: itm1.height
            width: itm1.width
            background: Rectangle{
                border.color: "#1588b5"
                radius: 10
            }
            font {
                pixelSize: spnb_font.value
            }
            wrapMode: wrapcheck.checked ? TextEdit.WordWrap : TextEdit.NoWrap




        }
        ScrollBar.vertical: ScrollBar{}
        ScrollBar.horizontal: ScrollBar{}
    }
}


}
