import QtQuick 2.12
import QtQuick.Controls 2.5
import QtWebView 1.1

Page {
    header: Item {
        width: parent.width
        height: 40
        Rectangle {
            anchors.fill: parent
            color: "#1588b5"
            Button {
                anchors.left: parent.left
                anchors.top: parent.top
                height: parent.height
                width: parent.height
    //            anchors.margins: 10

                id:h_btn_burger
                icon.source: "qrc:/Media/Icons/back.png"
                icon.color: "white"
                background: Rectangle {
                    color: h_btn_burger.pressed ? "#155a75":"#1588b5"
                }
                onClicked: {
                    swipeView.currentIndex = 0
                }


            }
            Text {
                id: rtcg_h
                text: qsTr("Предварительный просмотр")
                color: "white"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 20
                font.bold: true
            }
            Button {
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.height
                width: parent.height
    //            anchors.margins: 10

                id:h_btn_rel
                icon.source: "qrc:/Media/Icons/refresh.png"
                icon.color: "white"
                background: Rectangle {
                    color: h_btn_rel.pressed ? "#155a75":"#1588b5"
                }
                onClicked: {
                    webv.reload();
                }


            }
        }


    }


    WebView {
        id:webv
            anchors.fill: parent
            url: "D:/Work/MosPolytech/2.5 year/Mobile Development/Qt/build-181_331_Zhirkov-Desktop_Qt_5_14_1_MSVC2017_64bit-Debug/temp.html"
        }
}
