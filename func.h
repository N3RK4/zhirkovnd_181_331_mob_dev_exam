#ifndef FUNC_H
#define FUNC_H

#include <QObject>
#include <QFile>
#include <QString>
#include <QFile>
#include <QTextStream>

class func : public QObject
{
    Q_OBJECT
public:
    explicit func(QObject *parent = nullptr);

public slots:
    void toSave(QString tta, QString fp);
    void toSave2(QString tta);

};

#endif // FUNC_H
